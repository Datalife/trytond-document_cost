datalife_document_cost
======================

The document_cost module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-document_cost/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-document_cost)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
